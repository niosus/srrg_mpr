#pragma once

#include "srrg_boss/eigen_boss_plugin.h"  // this goes before all eigen stuff
#include <Eigen/Geometry>
#include <Eigen/StdVector>
#include <srrg_boss/serializable.h>

namespace srrg_mpr {
using namespace srrg_boss;

struct MPRIterationStats : public srrg_boss::Serializable {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  Eigen::Isometry3f T = Eigen::Isometry3f::Identity();  // not saved in log
  float chi = 0;
  int num_inliers = 0;
  void serialize(ObjectData& data, IdContext& context) override;
  void deserialize(ObjectData& data, IdContext& context) override;
};

struct MPRLevelStats : public srrg_boss::Serializable {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  int rows = 0;
  int cols = 0;
  int cloud_size = 0;
  double time_projections = 0;
  double time_linearize = 0;
  std::vector<MPRIterationStats, Eigen::aligned_allocator<MPRIterationStats> >
      iterations;

  void serialize(ObjectData& data, IdContext& context) override;
  void deserialize(ObjectData& data, IdContext& context) override;
};

struct MPRStats : public std::vector<MPRLevelStats,
                                     Eigen::aligned_allocator<MPRLevelStats> >,
                  public srrg_boss::Serializable {
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  bool is_keyframe = false;
  double time_pyramid = 0;
  double timeProjections() const;
  double timeLinearize() const;
  Eigen::Isometry3f T = Eigen::Isometry3f::Identity();  // transform between
                                                        // this frame and the
                                                        // previousl
  Eigen::Isometry3f global_T = Eigen::Isometry3f::Identity();

  void serialize(ObjectData& data, IdContext& context) override;
  void deserialize(ObjectData& data, IdContext& context) override;
};
}
