#include "mpr_solver.h"
#include "mpr_utils.h"
#include "srrg_types/defs.h"

using namespace std;

// profiling at solver level is invasive, getTime takes substantial CPU
// enable this only if you need to measure the time of the jacobian
// #define __PROFILE_JACOBIAN__

namespace srrg_mpr {
using namespace srrg_core;

MPRSolver::MPRSolver() {
  _T = Eigen::Isometry3f::Identity();
  _camera_matrix = Eigen::Matrix3f::Identity();
  _H = Matrix6f::Identity();
  _b = Vector6f::Zero();
  _rows = 0;
  _cols = 0;
  _min_depth = 0;
  _max_depth = 0;
  _cloud = 0;
}

void MPRSolver::setCloud(const Cloud& cloud) {
  _cloud = &cloud;
  _point_states.resize(cloud.size());
}

void MPRSolver::setPyramidLevel(const MPRPyramidLevel& level) {
  _level_ptr = &level;
  _rows = _level_ptr->_rows;
  _cols = _level_ptr->_cols;
  _min_depth = _level_ptr->_min_depth;
  _max_depth = _level_ptr->_max_depth;
  _camera_matrix = _level_ptr->_camera_matrix;
  _camera_type = _level_ptr->_camera_type;
  _sensor_offset_inverse = _level_ptr->_sensor_offset.inverse();
  _sensor_offset_rotation_inverse = _sensor_offset_inverse.linear();
  _workspace.resize(_rows, _cols);
}

void MPRSolver::computeProjections() {
  _workspace.fill(WorkspaceEntry());

  // FloatImage depth_image;
  // depth_image.create(_rows, _cols);
  // depth_image = 0;

  const Cloud3D& cloud = *_cloud;
  Eigen::Isometry3f ST = _sensor_offset_inverse * _T;
  for (size_t i = 0; i < _cloud->size(); ++i) {
    PointStatus& status = _point_states[i];
    status._error.setZero();
    status._chi = 0.f;

    const RichPoint3D& full_point = cloud[i];
    const float intensity = full_point.rgb().x();
    const Eigen::Vector3f transformed_point = ST * full_point.point();

    float depth = 0.f;
    float inverse_depth = 1.f;
    Eigen::Vector3f camera_point = Eigen::Vector3f::Zero();
    Eigen::Vector2f image_point = Eigen::Vector2f::Zero();

    switch (_camera_type) {
      case MPRPyramidLevel::Pinhole:
        depth = transformed_point.z();
        if (depth < _min_depth || depth > _max_depth) {
          status._flag = DepthOutOfRange;
          continue;
        }
        inverse_depth = 1.0 / depth;
        camera_point = _camera_matrix * transformed_point;
        image_point = camera_point.head<2>() * inverse_depth;
        break;
      case MPRPyramidLevel::Spherical:
        depth = transformed_point.norm();
        if (depth < _min_depth || depth > _max_depth) {
          status._flag = DepthOutOfRange;
          continue;
        }
        // cerr << "g";
        camera_point.x() = atan2(transformed_point.y(), transformed_point.x());
        camera_point.y() =
            atan2(transformed_point.z(), transformed_point.head<2>().norm());
        camera_point.z() = depth;

        image_point.x() =
            _camera_matrix(0, 0) * camera_point.x() + _camera_matrix(0, 2);
        image_point.y() =
            _camera_matrix(1, 1) * camera_point.y() + _camera_matrix(1, 2);
        break;
      default: throw std::runtime_error("unknown camera type");
    }

    int irow = cvRound(image_point.y());
    int icol = cvRound(image_point.x());

    if (!_workspace.inside(irow, icol)) {
      status._flag = Outside;
      // cerr << "o";
      continue;
    }

    // check if masked
    if (!_level_ptr->_mask.empty() &&
        _level_ptr->_mask.at<unsigned char>(irow, icol)) {
      status._flag = Masked;
      // cerr << "m";
      continue;
    }

    WorkspaceEntry& entry = _workspace(irow, icol);

    if (entry._depth > depth) {
      if (entry._index > -1) { status._flag = Occluded; }
      // cerr << "e";
      entry._index = i;
      entry._point = transformed_point;
      entry._normal = ST.linear() * full_point.normal();
      entry._camera_point = camera_point;
      entry._image_point = image_point;
      entry._intensity = intensity;
      entry._depth = depth;
      status._flag = Good;
      // depth_image.at<float>(irow, icol) = depth/_level_ptr->max_depth;
    } else {
      status._flag = Occluded;
    }
  }
  // cv::imshow("porcata immonda", depth_image);
}

void MPRSolver::updateStats() {
  for (int i = 0; i < _num_point_states; ++i) _point_states_stats[i] = 0;

  for (size_t i = 0; i < _point_states.size(); ++i) {
    ++_point_states_stats[_point_states[i]._flag];
  }
}

MPRSolver::PointStatusFlag MPRSolver::errorAndJacobian(
    Vector5f& error, Matrix5_6f& jacobian, const WorkspaceEntry& entry) {
  using namespace std;

  PointStatusFlag status = Good;
  const float z = entry._depth;
  const float iz = 1.0f / z;
  const Eigen::Vector3f& point = entry._point;
  const Eigen::Vector3f& normal = entry._normal;
  const Eigen::Vector3f& camera_point = entry._camera_point;
  const Eigen::Vector2f& image_point = entry._image_point;
  jacobian.setZero();

  Vector5f measurement;
  Matrix5_2f image_derivatives;
  bool ok =
      _level_ptr->getSubPixel(measurement, image_derivatives, image_point);
  if (!ok) return Masked;

  // in error put the difference between prediction and measurement
  error = entry.prediction() - measurement;

  // if the distance between a point and the corresponding one is
  // too big, we drop
  if (std::pow(error(1), 2) > _config.depth_error_rejection_threshold)
    return DepthError;

  // compute the pose jacobian, including sensor offset
  // _sensor_offset.linear().transpose * [ I  -skew(point_in_camera_frame) ]

  // this is done in linearize and never changes
  // _J_icp.block<3,3>(0,0)=_sensor_offset_rotation_inverse;

  _J_icp.block<3, 3>(0, 3) =
      _sensor_offset_rotation_inverse * skew((const Eigen::Vector3f)-point);

  // extract values from hom for readability
  const float iz2 = iz * iz;

  // extract the valiues from camera matrix
  const float& fx = _camera_matrix(0, 0);
  const float& fy = _camera_matrix(1, 1);
  const float& cx = _camera_matrix(0, 2);
  const float& cy = _camera_matrix(1, 2);

  // computes J_hom*K explicitly to avoid matrix multiplication
  // and stores it in J_proj
  Eigen::Matrix<float, 2, 3> J_proj = Eigen::Matrix<float, 2, 3>::Zero();

  switch (_camera_type) {
    case MPRPyramidLevel::Pinhole:
      // fill the left  and the right 2x3 blocks of J_proj with J_hom*K
      J_proj(0, 0) = fx * iz;
      J_proj(0, 2) = cx * iz - camera_point.x() * iz2;
      J_proj(1, 1) = fy * iz;
      J_proj(1, 2) = cy * iz - camera_point.y() * iz2;

      // add the jacobian of depth prediction to row 1.
      jacobian.row(1) = _J_icp.row(2);

      break;
    case MPRPyramidLevel::Spherical: {
      const float r = z;
      const float ir = iz;
      const float ir2 = iz2;
      const float rxy2 = point.head<2>().squaredNorm();
      const float irxy2 = 1. / rxy2;
      const float rxy = sqrt(rxy2);
      const float irxy = 1. / rxy;

      J_proj << -fx * point.y() * irxy2,  // 1st row
          fx * point.x() * irxy2, 0,
          -fy * point.x() * point.z() * irxy * ir2,  // 2nd row
          -fy * point.y() * point.z() * irxy * ir2, fy * rxy * ir2;

      Eigen::Matrix<float, 1, 3> J_depth;  // jacobian of range(x,y,z)
      J_depth << point.x() * ir, point.y() * ir, point.z() * ir;

      // add the jacobian of range prediction to row 1.
      jacobian.row(1) = J_depth * _J_icp;
    } break;
  }
  // chain rule on projection + pose
  srrg_core::Matrix2_6f J_proj_icp = J_proj * _J_icp;

  // chain rule to get the jacobian
  jacobian.noalias() -= image_derivatives * J_proj_icp;

  // massage the jacobian by subtracting the derivatives of the projected
  // quantities

  // J_normal/dPert += skew(-predicted normal)
  jacobian.block<3, 3>(2, 3).noalias() +=
      _sensor_offset_rotation_inverse * skew((const Eigen::Vector3f)-normal);

  // Omega is diagonal matrix
  // to avoid multiplications we premultiply the rows of J by sqrt of diag
  // elements
  jacobian.row(0) *= _omega_intensity_sqrt;
  jacobian.row(1) *= _omega_depth_sqrt;
  jacobian.block<3, 2>(2, 0) *= _omega_normals_sqrt;

  // we do the same for the error
  error(0) *= _omega_intensity_sqrt;
  error(1) *= _omega_depth_sqrt;
  error.block<3, 1>(2, 0) *= _omega_normals_sqrt;
  return status;
}

inline void computeAtxA(Matrix6f& dest, const Matrix5_6f& src, float lambda) {
  for (int c = 0; c < 6; ++c)
    for (int r = 0; r <= c; ++r)
      dest(r, c) += src.col(r).dot(src.col(c)) * lambda;
}

inline void copyLowerTriangleUp(Matrix6f& A) {
  for (int c = 0; c < 6; ++c)
    for (int r = 0; r < c; ++r) A(c, r) = A(r, c);
}

void MPRSolver::linearize() {
  // refresh the square roots of omegas from config, just in case

  _omega_intensity_sqrt = std::sqrt(_config.omega_intensity);
  _omega_depth_sqrt = std::sqrt(_config.omega_depth);
  _omega_normals_sqrt = std::sqrt(_config.omega_normals);

  _H.setZero();
  _b.setZero();

  _total_chi = 0;
  _num_inliers = 0;
  float lambda = 1;
  Vector5f e;
  Matrix5_6f J;
  _J_icp.block<3, 3>(0, 0) = _sensor_offset_rotation_inverse;

  for (int r = 0; r < _workspace.rows(); ++r) {
    const WorkspaceEntry* entry_ptr = _workspace.rowPtr(r);
    for (int c = 0; c < _workspace.cols(); ++c, ++entry_ptr) {
      const WorkspaceEntry& entry = *entry_ptr;
      const int idx = entry._index;
      if (idx < 0) continue;

      PointStatus& status = _point_states[idx];
      if (status._flag != Good) continue;

      status._flag = errorAndJacobian(e, J, entry);
      if (status._flag != Good) { continue; }
      float chi = e.dot(e);
      status._chi = chi;
      status._error = e;
      lambda = 1;
      if (chi > _config.kernel_chi_threshold) {
        lambda = sqrt(_config.kernel_chi_threshold / chi);
      } else {
        _num_inliers++;
      }
      _total_chi += chi;

      computeAtxA(_H, J, lambda);
      _b.noalias() += J.transpose() * e * lambda;
    }
  }

  copyLowerTriangleUp(_H);

  if (_config.verbose) {
    std::cerr << "chi2:\t" << _total_chi << "\t num_inliers: " << _num_inliers
              << "\t_cloud_size: " << _cloud->size() << std::endl;
    cerr << "stats: ";
    for (int i = 0; i < _num_point_states; ++i)
      cerr << _point_states_stats[i] << "\t";
  }
}

void MPRSolver::compute() {
  checkCompute();
  _time_projections = getTime();
  computeProjections();
  _time_projections = getTime() - _time_projections;

  _time_linearize = getTime();
  linearize();
  _time_linearize = getTime() - _time_linearize;

  _H += Matrix6f::Identity() * _config.damping;

  // compute a solution
  Vector6f dx = _H.ldlt().solve(-_b);
  _T = v2tEuler(dx) * _T;
  // ensure that the computed linear part of _T
  // is a rotation matrix
  Eigen::Matrix3f R = _T.linear();
  Eigen::Matrix3f E = R.transpose() * R;
  E.diagonal().array() -= 1;
  _T.linear() -= 0.5 * R * E;
}

void MPRSolver::checkCompute() {
  if (_rows == 0 || _cols == 0)
    throw std::runtime_error("You forgot to setup image!");
  if (_max_depth == 0.f || _min_depth == 0.f)
    throw std::runtime_error("You forgot to setup min&max depth!");
  if (_config.damping == 0.f)
    std::cerr
        << "You leave the damping to 0.f by default\nMay the force be with you";
}

cv::Mat MPRSolver::getErrorImage(MPRPyramidLevel::ChannelType type) const {
  cv::Mat dest;
  int cv_type = 0;
  int offset = 0;
  int count = 1;

  switch (type) {
    case MPRPyramidLevel::Intensity:
      cv_type = CV_32FC1;
      offset = 0;
      count = 1;
      break;
    case MPRPyramidLevel::Depth:
      cv_type = CV_32FC1;
      offset = 1;
      count = 1;
      break;
    case MPRPyramidLevel::Normals:
      cv_type = CV_32FC3;
      count = 3;
      offset = 2;
      break;
  }
  dest.create(_rows, _cols, cv_type);
  dest = 0.f;
  int copied_elements = 0;
  for (int r = 0; r < _workspace.rows(); ++r) {
    const WorkspaceEntry* entry_ptr = _workspace.rowPtr(r);
    float* dest_ptr = dest.ptr<float>(r);
    for (int c = 0; c < _workspace.cols();
         ++c, ++entry_ptr, dest_ptr += count) {
      const WorkspaceEntry& entry = *entry_ptr;
      if (entry._index < 0) continue;
      const Vector5f& error = _point_states[entry._index]._error;
      for (int k = 0; k < count; ++k) { dest_ptr[k] = error[offset + k]; }
    }
  }
  return dest;
}

cv::Mat MPRSolver::getTiledErrorImage() const {
  std::vector<cv::Mat> error_stripe(3);
  error_stripe[0] =
      replicateChannels(getErrorImage(MPRPyramidLevel::Intensity));
  error_stripe[0] *= 0.5;
  error_stripe[0] += cv::Vec3f(0.5, 0.5, 0.5);
  error_stripe[1] = replicateChannels(getErrorImage(MPRPyramidLevel::Depth));
  error_stripe[1] *= 2;
  error_stripe[1] += cv::Vec3f(0.5, 0.5, 0.5);
  error_stripe[2] = getErrorImage(MPRPyramidLevel::Normals);
  error_stripe[2] *= 0.5;
  error_stripe[2] += cv::Vec3f(0.5, 0.5, 0.5);
  cv::Mat dest;
  tileImages(dest, error_stripe, true);
  return dest;
}
}
