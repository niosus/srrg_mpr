#pragma once
#include "mpr_pyramid_generator.h"
#include "mpr_solver.h"
#include "mpr_stats.h"
namespace srrg_mpr {
using namespace std;

typedef std::vector<MPRSolver, Eigen::aligned_allocator<MPRSolver> >
    MPRSolverVector;

class MPRAligner {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  struct Config {
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;
    std::vector<int> levels_iterations;
    int hysteresis_size = 3;
    MPRPyramidGenerator::Config pyramid;
    MPRSolver::Config solver;
  };

  // this has to be called explicitly after construction
  virtual void setup();

  virtual ~MPRAligner();

  Config& mutableConfig();

  const Config& constConfig() const;

  inline const int numLevels() const {
    return constConfig().levels_iterations.size();
  }

  virtual void setFixedImages(const srrg_core::RawDepthImage& raw_depth,
                              const srrg_core::RGBImage& raw_rgb);

  virtual void setMovingImages(const srrg_core::RawDepthImage& raw_depth,
                               const srrg_core::RGBImage& raw_rgb);

  inline const Eigen::Isometry3f& T() const { return _T; }

  //! this sets the workspace based on the configuration
  virtual void init();

  virtual void compute();

  inline const MPRStats& stats() const { return _stats; }

  MPRPyramid _fixed_pyramid;
  MPRPyramid _moving_pyramid;

 protected:
  // constructs a pyramid in the generator, using raw_depth and raw rgb images
  const MPRPyramid& buildPyramid(const srrg_core::RawDepthImage& raw_depth,
                                 const srrg_core::RGBImage& raw_rgb);

  // performs the alignment, of moving vs fixed using _T as initial guess
  // result in _T
  void align(const MPRPyramid& fixed_pyramid, const MPRPyramid& moving_pyramid);

  Config* _config_ptr = nullptr;  // instantiated with the setup()
  MPRStats _stats;
  MPRPyramidGenerator _pyramid_generator;
  MPRSolverVector _solvers;

  srrg_core::RGBImage _rgb_fixed_raw;
  srrg_core::RGBImage _rgb_moving_raw;

  srrg_core::RawDepthImage _depth_fixed_raw;
  srrg_core::RawDepthImage _depth_moving_raw;

  bool _is_ready = false;
  Eigen::Isometry3f _T = Eigen::Isometry3f::Identity();
  double _time_pyramid;
  double _time_projections;
  double _time_linearize;

  float _best_chi;
  Eigen::Isometry3f _best_T;
  std::vector<bool> _higher_chi;
};
}
